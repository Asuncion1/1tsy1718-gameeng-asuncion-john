﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MonsterType
{
	Flying, Ground
}
	

public class SpawnManager : MonoBehaviour {
	public List<GameObject> monstersToSpawn = new List<GameObject> ();
	public List<GameObject> monsterWave = new List<GameObject> ();
	public int 				remainingMonsters;
	public int 				waveNumber;
	public int 				waveSize;
	public GameObject 		monsterTarget;
	bool 					bossWave;
	// Use this for initialization
	void Start () 
	{
		monstersToSpawn [0].GetComponent<EnemyController> ().target = monsterTarget;
		StartCoroutine (Spawn () );
		bool bossWave = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		waveSize = monsterWave.Count;

		if (monsterWave.Count <= 0 && remainingMonsters <= 0)
		{
			//Reset Wave
			waveNumber++;
			remainingMonsters = 5;
			bossWave = 			true;
		}



		foreach (var monster in monsterWave.ToArray())
		{
			if (monster.GetComponent<EnemyController>().isDead == true) 
			{
				Destroy (monster.gameObject);
				monsterWave.Remove (monster);
			} 
		}
	
	}
	IEnumerator Spawn()
	{
		while (true) 
		{
			if (remainingMonsters != 0) 
			{
				GameObject monster = Instantiate (monstersToSpawn[0], this.transform.position, monstersToSpawn [0].transform.rotation);
				monsterWave.Add (monster);
				monster.GetComponent<MonsterData> ().construct (waveNumber);
			
				Debug.Log ("Spawned");
				remainingMonsters--;
			}
			if (waveNumber % 5 == 0 && bossWave == true) 
			{
					//Spawn Boss
				GameObject boss = Instantiate (monstersToSpawn[0], this.transform.position, monstersToSpawn [0].transform.rotation);
				monsterWave.Add (boss);
				boss.GetComponent<MonsterData> ().constructBoss (waveNumber);
				boss.gameObject.tag = "Boss";
				bossWave = false;
			}
			yield return new WaitForSeconds (1);
		}
	}
}
