﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroSpawner : MonoBehaviour {
	public List<GameObject> heroes;
	public HeroSkills 		hero;
	void Awake()
	{
		switch (PlayerPrefs.GetInt("hero")) 
		{
		case 0:
			Instantiate (heroes[0], this.transform.position, heroes [0].transform.rotation);
			break;
		case 1:
			Instantiate (heroes[1], this.transform.position, heroes [1].transform.rotation);
			break;
		case 2:
			Instantiate (heroes[2], this.transform.position, heroes [2].transform.rotation);
			break;
		default:
			break;
		}
	}

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
