﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ConstructionManager : MonoBehaviour {

	Ray 						ray;
	RaycastHit 					hit;
	public List<GameObject> 	towerPrefab = new List<GameObject> ();
	public GameObject 			selectedTower;
	public TowerManager 		towerManager;

	TowerData.TowerTypes 		currentTower;
	// Use this for initialization
	void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{
		
		planTower ();

		Debug.DrawLine (ray.origin, hit.point, Color.red);
	}

	public void selectTower(int towerNum)
	{
		//Build Delay
		foreach (var item in towerManager.towers) 
		{
			if (item.GetComponent<TowerData>().isBuilding) 
			{
				return;
			}
		}
		currentTower = (TowerData.TowerTypes)towerNum;
		createTower ();
		Debug.Log ((int)currentTower);
	}

	public void createTower()
	{
		if (selectedTower == null) 
		{
			selectedTower = Instantiate (towerPrefab [(int)currentTower] as GameObject, hit.point, towerPrefab [(int)currentTower].transform.rotation);
			selectedTower.GetComponent<TowerData> ().construct (currentTower);
			towerManager.towers.Add (selectedTower);
		}
	}

	public void planTower()
	{
		if (selectedTower != null) 
		{
			ray = Camera.main.ViewportPointToRay (Camera.main.ScreenToViewportPoint (Input.mousePosition));
			if (Physics.Raycast (ray, out hit, Mathf.Infinity)) 
			{
				if (selectedTower != null) 
				{
					selectedTower.transform.position = 						SnapToGrid(hit.point);
					selectedTower.GetComponent<TowerData> ().isPlanning = 	true;
				}
	
			}
			
			if (hit.point.y > 2) 
			{
				selectedTower.GetComponent<TowerScript> ().Buildable ();
				if (Input.GetMouseButtonUp (0) && !EventSystem.current.IsPointerOverGameObject ()) 
				{
					selectedTower.GetComponent<TowerScript> ().Build ();
					selectedTower.GetComponent<TowerData> ().isBuilding = true;
					selectedTower.GetComponent<TowerData> ().isPlanning = false;
					selectedTower = null;
				}
			} 
			else 
			{
				selectedTower.GetComponent<TowerScript> ().NonBuildable ();
			}
		}
	}

	Vector3 SnapToGrid(Vector3 towerObject)
	{
		return new Vector3(Mathf.Round(towerObject.x), towerObject.y, Mathf.Round(towerObject.z));
	}
}
