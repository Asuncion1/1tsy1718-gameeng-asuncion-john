﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	public float speed;
	public float offset;
	Vector3 pos;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		pos = this.transform.position;
		if (Input.mousePosition.x >= Screen.width - offset ) 
		{
			this.transform.position += (Vector3.right * speed * Time.deltaTime);
		}
		if (Input.mousePosition.x <= offset) 
		{
			this.transform.position += (Vector3.right * -speed * Time.deltaTime);
		}
		if (Input.mousePosition.y >= Screen.height - offset ) 
		{
			this.transform.position += (Vector3.forward * speed * Time.deltaTime);
		}	
		if (Input.mousePosition.y <= offset ) 
		{
			this.transform.position += (Vector3.forward * -speed * Time.deltaTime);
		}

		pos.x = Mathf.Clamp (this.transform.position.x, 23, 43);
		pos.z = Mathf.Clamp (this.transform.position.z, 10, 51);
		transform.position = pos;


	}
}