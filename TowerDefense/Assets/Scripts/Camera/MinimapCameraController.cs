﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCameraController : MonoBehaviour {
	Camera 				camera;
	public GameObject 	mainCamera;
	Vector3 			pos;
	Vector3 			mousePos;
	Vector3				viewPort;
	public float 		mapMaxSize;
	// Use this for initialization
	void Start () 
	{
		camera = GetComponent<Camera> ();	
	}
	
	// Update is called once per frame
	void Update () 
	{
		viewPort = camera.ScreenToViewportPoint (Input.mousePosition);
		pos = mainCamera.transform.position;
		RaycastHit hit;
		Ray ray = camera.ViewportPointToRay (viewPort);
		if (Physics.Raycast (ray, out hit, Mathf.Infinity) && Input.GetMouseButton(0)) 
		{

			Debug.Log (hit.point);
			pos.x = hit.point.x;
			pos.z = hit.point.z;

			mainCamera.transform.position = pos;

		}
		Debug.DrawLine (transform.position, hit.point );
		Debug.DrawLine (transform.position, new Vector3(transform.position.x, transform.position.y * -10, transform.position.z), Color.blue);
		camera.orthographicSize = (mapMaxSize * camera.rect.height);
	}
		
}
