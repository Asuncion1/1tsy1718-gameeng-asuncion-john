﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroProjectileController : MonoBehaviour {

	public float speed;
	public GameObject target;
	public Rigidbody rigidbody;
	public HeroData hero;

	// Use this for initialization
	void Start () 
	{
		rigidbody = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () 
	{

	}

	void FixedUpdate()
	{
		if (target != null) 
		{
			Vector3 dir = target.transform.position - transform.position;
			rigidbody.velocity = dir.normalized * speed;
		} 
		else 
		{
			Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag.Contains("Enemy") || col.tag.Contains("Boss") ) 
		{
			col.gameObject.GetComponent<MonsterData> ().monsterHp -= hero.damage;
			Destroy(this.gameObject);
		}
	

	}
}
