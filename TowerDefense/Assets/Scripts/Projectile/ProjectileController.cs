﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {
	public float speed;
	public GameObject target;
	public Rigidbody rigidbody;
	public TowerData tower;
	Collider[] enemy;
	public LayerMask layer;
	// Use this for initialization
	void Start () 
	{
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void FixedUpdate()
	{
		if (target != null) 
		{
			if (tower.damageType == TowerData.TowerDamageType.splash) 
			{
				enemy = Physics.OverlapSphere (this.transform.position, 6, layer);
			}
			Vector3 dir = target.transform.position - transform.position;
			rigidbody.velocity = dir.normalized * speed;
		} 
		else 
		{
			Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag.Contains("Enemy") || col.tag.Contains("Boss") && tower.damageType != TowerData.TowerDamageType.splash) 
		{
			col.gameObject.GetComponent<MonsterData> ().monsterHp -= tower.damage;
			col.gameObject.GetComponent<MonsterData> ().debuffType = tower.debuffType;
			col.gameObject.GetComponent<MonsterData> ().isDebuffed = true;
			Destroy(this.gameObject);
		}
		if (col.tag.Contains("Enemy") || col.tag.Contains("Boss") && tower.damageType == TowerData.TowerDamageType.splash) 
		{
			if (enemy != null) 
			{
				foreach (var item in enemy) 
				{
					if (enemy != null) 
					{
						if (item != null) 
						{
							item.gameObject.GetComponent<MonsterData> ().monsterHp -= tower.damage;
							item.gameObject.GetComponent<MonsterData> ().debuffType = tower.debuffType;
							item.gameObject.GetComponent<MonsterData> ().isDebuffed = true;
						}
					}
				}
			}

			Destroy(this.gameObject);
		}

	}


}
