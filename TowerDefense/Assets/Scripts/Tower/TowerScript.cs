﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TowerScript : MonoBehaviour {

	[SerializeField]Material 	towerMat;
	public SpawnManager 		spawnManager;
	public ConstructionManager 	constructMan;
	public TowerData 			towerData;
	public GameObject 			target;
	public GameObject 			turret;
	public GameObject 			bullet;
	float countDown = 			0;
	// Use this for initialization
	void Start () 
	{
		towerData = GetComponent<TowerData> ();
		spawnManager = GameObject.Find ("SpawnManager").GetComponent<SpawnManager>();
		InvokeRepeating ("AddTargets", 0f, 0.5f);
	}

	
	// Update is called once per frame
	void Update () 
	{
		//AddTargets (towerData);
		if (towerData.isBuilding == false && towerData.isPlanning == false && target != null) 
		{
			if (towerData.type == TowerData.TowerTypes.cannonTower && target.gameObject.GetComponent<MonsterData>().type == MonsterType.Ground) 
			{
				RotateTurret();
				FireProjectile (); 
			}
			if (towerData.type != TowerData.TowerTypes.cannonTower) 
			{
				RotateTurret();
				FireProjectile ();
			}
		}

	}

	public void Buildable()
	{
		towerMat.color = Color.green;
	}

	public void NonBuildable()
	{
		towerMat.color = Color.red;
	}

	public void Build()
	{
		towerMat.color = Color.white;
	}

	void AddTargets()
	{
		float currentTargetDistance = Mathf.Infinity;
		GameObject nearestTarget = null;
		foreach (var item in spawnManager.monsterWave)
		{
			float currentItemDistance = Vector3.Distance (this.transform.position, item.transform.position);


			if (towerData.type == TowerData.TowerTypes.arrowTower || towerData.type == TowerData.TowerTypes.cannonTower) {
				if (currentItemDistance <= (int)towerData.range && item.gameObject.GetComponent<EnemyController> ().isDead == false) 
				{
					target = item;
					//Debug.Log ("First Target in Range Received");
					return;
				} 
				else 
				{
					target = null;
				}
			} 
			else if (towerData.type == TowerData.TowerTypes.iceTower || towerData.type == TowerData.TowerTypes.fireTower) 
			{
				
				if (currentItemDistance < currentTargetDistance) 
				{
					//Debug.Log ("Closest Target in Range Received");
					currentTargetDistance = currentItemDistance;
					nearestTarget = item;
					//return;
				}
			}

		}
		if (currentTargetDistance <= (int)towerData.range && nearestTarget.gameObject.GetComponent<EnemyController> ().isDead == false && nearestTarget != null) 
		{
			target = nearestTarget;
		} 
		else 
		{
			target = null;
		}
	}
		
	void RotateTurret()
	{
		if (target != null) 
		{
			Vector3 dir = target.transform.position - this.transform.position;
			Quaternion lookRot = Quaternion.LookRotation (dir);
			Vector3 rot = lookRot.eulerAngles;
			turret.transform.rotation = Quaternion.Euler (0f, rot.y, 0f);
		}
		//turret.transform.rotation = target.transform.rotation;
	}

	void FireProjectile()
	{
		
		if (target != null) 
		{
			if(countDown <= 0)
			{
				//Debug.Log ("Fire");
				GameObject projectile = Instantiate (bullet, this.transform.position, bullet.transform.rotation);
				projectile.gameObject.GetComponent<ProjectileController> ().target = target;
				projectile.gameObject.GetComponent<ProjectileController> ().tower = towerData;
				countDown = 1f / (float)towerData.fireRate;
			}
			countDown -= Time.deltaTime;
		}
	}

	void OnDrawGizmosSelected() 
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(this.transform.position, (int)towerData.range);
	}

}
