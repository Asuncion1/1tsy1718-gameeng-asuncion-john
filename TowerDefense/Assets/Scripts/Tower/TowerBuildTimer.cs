﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerBuildTimer : MonoBehaviour {

	public Slider 		slider;
	//public Text text;
	public TowerData 	tower;

	// Use this for initialization
	void Start () 
	{
		slider = 			GetComponent<Slider> ();
		slider.maxValue = 	tower.buildTime;
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.transform.rotation = Camera.main.transform.rotation;
		if (tower.isBuilding && tower.isPlanning == false) 
		{
			slider.value = 		tower.buildTime;
			tower.buildTime -= 	Time.deltaTime;
			if (tower.buildTime <= 0) 
			{
				Debug.Log ("Construction Done");
				tower.isBuilding = 	false;
				tower.buildTime = 	0;
				Destroy (this.gameObject);
			}
		}

		//text.text = Mathf.Round (slider.value).ToString();
	}
}
