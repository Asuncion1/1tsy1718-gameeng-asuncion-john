﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerData : MonoBehaviour {

	public enum TowerTypes
	{
		arrowTower = 0,
		cannonTower = 1,
		iceTower = 2,
		fireTower = 3
	}
	public enum TowerRange
	{
		shortRange = 10,
		mediumRange = 15,
		longRange = 20
	}
	public enum TowerFireRate
	{
		slow = 1,
		medium = 2,
		fast = 4
	}
	public enum TowerDamageType
	{
		normal,
		splash
	}
	public int 					price;
	public string 				name;
	public float 				buildTime;
	public int 					damage;
	public TowerTypes 			type;
	public TowerRange 			range;
	public TowerFireRate 		fireRate;
	public Debuff.DebuffType 	debuffType;
	public TowerDamageType 		damageType;
	public bool 				isBuilding;
	public bool 				isPlanning;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	public void construct(TowerTypes type)
	{
		isBuilding = false;
		if (type == TowerTypes.arrowTower) 
		{
			name = 			"Arrow";
			price = 		100;
			buildTime = 	10;
			range = 		TowerRange.longRange;
			fireRate = 		TowerFireRate.fast;
			damage = 		5;
			damageType = 	TowerDamageType.normal;
		}
		else if (type == TowerTypes.cannonTower)
		{
			name = 			"Cannon";
			price = 		200;
			buildTime = 	15;
			range = 		TowerRange.shortRange;
			fireRate = 		TowerFireRate.slow;
			damage = 		5;
			damageType = 	TowerDamageType.splash;
		}
		else if (type == TowerTypes.iceTower)
		{
			name = 			"Ice";
			price = 		500;
			buildTime = 	30;
			range = 		TowerRange.mediumRange;
			fireRate = 		TowerFireRate.medium;
			debuffType = 	Debuff.DebuffType.chilled;
			damage = 		5;
			damageType = 	TowerDamageType.splash;
		}
		else if (type == TowerTypes.fireTower)
		{
			name = 			"Fire";
			price = 		500;
			buildTime = 	30;
			range = 		TowerRange.mediumRange;
			fireRate = 		TowerFireRate.medium;
			debuffType = 	Debuff.DebuffType.burning;
			damage = 		5;
			damageType = 	TowerDamageType.splash;
		}
	}
}
