﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterData : MonoBehaviour {

	public string 				name;
	public MonsterType 			type;
	public GameObject 			monsterPrefab;
	public float 				monsterHp;
	public float 				monsterMaxHp;
	public float 				gold;
	public Debuff.DebuffType 	debuffType;
	public Material 			monsterMat;
	public bool 				isDebuffed;
	public float 				maxSpeed = 8;

	public void construct (int wave)
	{
		gold *= 			wave;
		monsterMaxHp *= 	wave;
		monsterHp = 		monsterMaxHp;
		debuffType = 		Debuff.DebuffType.none;
		monsterMat.color = 	Color.white;
	}

	public void constructBoss(int wave)
	{
		name = 			"BOSS";
		monsterMaxHp = 	100 * wave;
		monsterHp = 	monsterMaxHp;
		gold = 			500 * wave;
		debuffType = 	Debuff.DebuffType.none;
	}
}
