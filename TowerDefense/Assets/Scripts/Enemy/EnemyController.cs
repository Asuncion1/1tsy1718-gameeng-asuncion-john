﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {
	public GameObject 	target;
	public MonsterData 	monsterData;
	Animator 			animator;
	NavMeshAgent 		agent;
	public bool 		isDead;


	// Use this for initialization
	void Start () 
	{
		agent = 		GetComponent<NavMeshAgent> ();
		animator = 		GetComponent<Animator> ();
		monsterData = 	GetComponent<MonsterData> ();
		agent.SetDestination (target.transform.position);
		isDead = 		false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if (agent.remainingDistance > 0) 
		{
			animator.SetFloat ("AnimIndex", 1);	
		} 
		if (agent.remainingDistance <= 2) 
		{
			animator.SetFloat ("AnimIndex", 0);	
		}
		if (monsterData.monsterHp <= 0) 
		{
			isDead = true;
		}
		if (monsterData.isDebuffed == true) 
		{
			monsterData.isDebuffed = false;
			if ( !this.GetComponent<ChilledDebuff>() && monsterData.debuffType == Debuff.DebuffType.chilled) 
			{
				this.gameObject.AddComponent<ChilledDebuff> ();
			}
			if (!this.GetComponent<BurningDebuff>() && monsterData.debuffType == Debuff.DebuffType.burning) 
			{
				this.gameObject.AddComponent<BurningDebuff> ();
			}

		}

	}

}
