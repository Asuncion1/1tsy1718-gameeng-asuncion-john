﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour {
	Slider 				slider;
	public MonsterData 	enemy;
	// Use this for initialization
	void Start () {
		slider = GetComponent<Slider> ();
		slider.maxValue = enemy.monsterHp;
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.rotation = Camera.main.transform.rotation;
		slider.value = enemy.monsterHp;
	}
}
