﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour {
	public CoreController 		core;
	public SpawnManager 		spawnManager;
	public Slider 				healthBar;
	public Text 				goldAmount;
	public Text 				wave;
	int 						gold;
	public List<SkillButton> 	buttonSkills;
	public PlayerController 	player;
	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindObjectOfType<PlayerController>();
		healthBar.maxValue = core.maxCoreHealth;
		gold = 500;

		if (player != null) 
		{
			for (int i = 0; i < buttonSkills.Count; i++) 
			{
				buttonSkills [i].skill = player.GetComponent<HeroSkills> ().skills [i];
			}
		}

	}
	
	// Update is called once per frame
	void Update () 
	{
		healthBar.value = core.coreHealth;
		goldAmount.text = gold.ToString();
		wave.text = "Wave " + spawnManager.waveNumber.ToString();

	}
}
