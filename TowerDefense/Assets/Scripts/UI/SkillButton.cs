﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour, IPointerClickHandler {
	public Skill skill;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (skill != null) 
		{
			skill.updateSkill ();
			skill.updateCooldown ();
			if (skill.onCooldown)
			{
				GetComponent<Image> ().fillAmount = skill.currentCooldown / skill.cooldown;	
				//Debug.Log ( 1 / skill.cooldown);
			}
		}


	}

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		if (skill != null && skill.onCooldown == false) 
		{
			GetComponent<Image> ().fillAmount = 0;
			skill.Activate ();	
		}
	}

	#endregion
}
