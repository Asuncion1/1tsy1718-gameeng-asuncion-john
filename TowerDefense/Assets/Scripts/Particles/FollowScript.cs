﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowScript : MonoBehaviour {
	public GameObject target;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (target != null) 
		{
			if (target.gameObject.GetComponent<MonsterData>().isDebuffed == false ) 
			{
				Destroy (this.gameObject);	
			}
			if (target.gameObject.GetComponent<MonsterData>().isDebuffed == true ) 
			{
				this.transform.position = target.transform.position;
			}
		}
		if (target == null) 
		{
			Destroy (this.gameObject);	
		}
	}
}
