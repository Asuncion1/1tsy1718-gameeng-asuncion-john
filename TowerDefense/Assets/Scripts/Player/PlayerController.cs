﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour {
	public Vector3 targetPos;
	NavMeshAgent agent;
	Animator animator;
	RaycastHit hit;
	public GameObject pointer;
	Vector3 viewPort;
	public SpawnManager spawnManager;
	public HeroData heroData;
	public GameObject target;
	public float countDown;
	// Use this for initialization
	void Start () 
	{
		target 			= null;
		spawnManager 	= GameObject.Find ("SpawnManager").GetComponent<SpawnManager>();
		agent 			= GetComponent<NavMeshAgent> ();
		animator 		= GetComponent<Animator> ();
		heroData 		= GetComponent<HeroData> ();
		InvokeRepeating ("AddTargets", 0f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		viewPort = Camera.main.ScreenToViewportPoint (Input.mousePosition);
		Ray ray = Camera.main.ViewportPointToRay (viewPort);

		if ( (Physics.Raycast(ray, out hit, 100)  && Input.GetMouseButtonDown(0)) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
		{
			agent.destination = hit.point;
			GameObject tempPointer;
			tempPointer = Instantiate (pointer, new Vector3(hit.point.x, hit.point.y + 2, hit.point.z), pointer.transform.rotation);
			Destroy (tempPointer.gameObject, 1);
		}
		//float distance = Vector3.Distance(this.transform.position, hit.point);

		Debug.DrawLine (this.transform.position, agent.destination);
		if (agent.remainingDistance > 0) 
		{
			heroData.state = HeroData.HeroState.moving;
			animator.SetFloat ("AnimIndex", 1);	
		} 
		if (agent.remainingDistance <= 2) 
		{
			heroData.state = HeroData.HeroState.idle;
			animator.SetFloat ("AnimIndex", 0);	
		}
		if (target != null && heroData.state == HeroData.HeroState.idle) 
		{
			FireProjectile ();
		}
		//Debug.Log (distance);
	}
	void AddTargets()
	{
		float currentTargetDistance = Mathf.Infinity;
		GameObject nearestTarget = null;
		foreach (var item in spawnManager.monsterWave)
		{
			float currentItemDistance = Vector3.Distance (this.transform.position, item.transform.position);

				if (currentItemDistance < currentTargetDistance) 
				{
					//Debug.Log ("Closest Target in Range Received");
					currentTargetDistance = currentItemDistance;
					nearestTarget = item;
					//return;
				}

		}
		if (currentTargetDistance <= (int)heroData.range && nearestTarget.gameObject.GetComponent<EnemyController> ().isDead == false && nearestTarget != null) 
		{
			target = nearestTarget;
		} 
		else 
		{
			target = null;
		}
	}
	void FireProjectile()
	{

		if (target != null) 
		{
			if(countDown <= 0)
			{
				Debug.Log ("Fire");
				GameObject projectile = Instantiate (heroData.projectile, this.transform.position, heroData.projectile.transform.rotation);
				projectile.gameObject.GetComponent<HeroProjectileController> ().target = target;
				projectile.gameObject.GetComponent<HeroProjectileController> ().hero = heroData;
				countDown = 1f / (float)heroData.fireRate;
			}
			countDown -= Time.deltaTime;
		}
	}
	void OnDrawGizmosSelected() 
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(this.transform.position, (int)heroData.range);
	}
}
