﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroData : MonoBehaviour {
	public enum HeroState
	{
		idle,
		moving
	}
	public enum HeroFireRate
	{
		slow = 1,
		medium = 2,
		fast = 4
	}
	public enum HeroRange
	{
		shortRange = 10,
		mediumRange = 15,
		longRange = 20
	}
	public HeroRange 		range;
	public HeroFireRate 	fireRate;
	public HeroState 		state;
	public int 				damage;
	public GameObject 		projectile;
	public string 			heroName;
}
