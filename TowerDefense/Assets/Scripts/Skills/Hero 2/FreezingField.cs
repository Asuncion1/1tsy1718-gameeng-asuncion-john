﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName="Skills/Crystal Blondy/Freezing Field")]
public class FreezingField : Skill {

	public MonsterData[] 	targets;
	public LayerMask 		layer;
	public override void Effect ()
	{
		
		targets = FindObjectsOfType<MonsterData> ();
		foreach (var item in targets) 
		{
			if (item != null) 
			{
				item.GetComponent<MonsterData> ().monsterHp -= item.GetComponent<MonsterData> ().monsterMaxHp * .05f;
				item.GetComponent<MonsterData> ().isDebuffed = true;
				item.GetComponent<MonsterData> ().debuffType = Debuff.DebuffType.chilled;
			}
		}
		base.Effect ();
	}
	public override void Activate()
	{
		Effect ();
	}

}
