﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName="Skills/Crystal Blondy/Crystal Nova")]
public class CrystalNova : Skill {
	public override void Effect ()
	{
		skillObject = new GameObject ();
		skillObject.transform.position = currentRangePrefab.transform.position;

		targets = Physics.OverlapSphere (currentRangePrefab.transform.position, rangeFinderSize, layer);
		if (skillObject != null) 
		{
			particleEffects = Instantiate(Resources.Load("Prefab/IceBlastParticles") as GameObject, skillObject.transform);
			ParticleSystem ps = particleEffects.GetComponent<ParticleSystem> ();
			var sh = ps.shape;
		}
		foreach (var item in targets) 
		{
			if (item != null) 
			{
				if (item.gameObject.CompareTag("Enemy")) 
				{
					item.GetComponent<MonsterData> ().monsterHp -= item.GetComponent<MonsterData> ().monsterMaxHp * .2f;
				}
				else if (item.gameObject.CompareTag("Boss")) 
				{
					item.GetComponent<MonsterData> ().monsterHp -= item.GetComponent<MonsterData> ().monsterMaxHp * .05f;
				}
				item.GetComponent<MonsterData> ().isDebuffed = true;
				item.GetComponent<MonsterData> ().debuffType = Debuff.DebuffType.chilled;
			}
		}
		base.Effect ();
	}

	public override void updateSkill()
	{
		if (skillObject != null) 
		{
			if (particleEffects != null)
			{
				particleEffects.transform.position = skillObject.transform.position;
			}
		}
	}
}
