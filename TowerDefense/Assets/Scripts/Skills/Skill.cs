﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Skill : ScriptableObject {
	[Header("General")]
	public float 		cooldown;
	public float 		currentCooldown;
	public float 		rangeFinderSize;
	public float 		range;
	public string 		name;
	public GameObject 	rangePrefab;
	public bool 		onCooldown;
	public GameObject 	skillObject;
	public Collider[] 	targets;
	public LayerMask 	layer;
	[SerializeField]
	public GameObject 	currentRangePrefab;
	public Vector3		rangePrefabPrevLoc;
	public GameObject 	target;
	public GameObject 	owner;
	Collider[] 			enemy;
	public Ray 			ray;
	public RaycastHit 	hit;
	[Header("Effects")]
	public int 			damage;
	public float		skillDuration;
	public GameObject 	particleEffects;
	[Range(0,1)]
	public float 		slowMultiplier;

	public void Awake()
	{
		onCooldown = false;
		Debug.Log (onCooldown);
		currentCooldown = cooldown;
	}

	public virtual void Effect()
	{
		onCooldown = true;
		RangeDisplay.OnActive -= Effect;
		Debug.Log (name);
	}
	public virtual void Activate()
	{
		if (rangeFinderSize > 0) 
		{
			currentRangePrefab = Instantiate (rangePrefab);
			currentRangePrefab.GetComponent<RangeDisplay> ().radius = rangeFinderSize;
			Debug.Log (currentRangePrefab.transform.position);
		}


		RangeDisplay.OnActive += Effect;

	}
	public virtual void updateSkill()
	{
		

	}
	public virtual void updateCooldown()
	{
		if (onCooldown) 
		{
			currentCooldown += Time.deltaTime;
		} 
		if(currentCooldown >= cooldown || onCooldown == false) 
		{
			currentCooldown = 0;
			onCooldown = false;
		}
	}
}
