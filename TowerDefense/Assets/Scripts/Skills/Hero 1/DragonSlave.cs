﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName="Skills/Lina/Dragon Slave")]
public class DragonSlave : Skill {
	public Vector3 	newPos;
	public float 	delay;
	public override void Effect ()
	{
		skillObject = new GameObject ();
		skillObject.transform.position = owner.transform.position;
	
		if (skillObject != null) 
		{
			particleEffects = Instantiate(Resources.Load("Prefab/TorchFireParticles") as GameObject, skillObject.transform);
			ParticleSystem ps = particleEffects.GetComponent<ParticleSystem> ();
			var sh = ps.shape;
		}
		base.Effect ();
	}

	void OnDrawGizmos() 
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(rangePrefab.transform.position, rangeFinderSize);
	}
		

	public override void updateSkill()
	{
		if (currentRangePrefab != null) 
		{
			rangePrefabPrevLoc = currentRangePrefab.transform.position;
		}
		if (skillObject != null) 
		{
			targets = Physics.OverlapSphere (skillObject.transform.position, rangeFinderSize, layer);
			Vector3 distance = (rangePrefabPrevLoc - skillObject.transform.position);
			skillObject.transform.position += new Vector3(distance.x, distance.y, distance.z) * 1 * Time.deltaTime;
			if (distance.magnitude <= 1) 
			{
				Destroy (skillObject.gameObject);
			}
			if (particleEffects != null)
			{
				particleEffects.transform.position = skillObject.transform.position;
			}
			foreach (var item in targets) 
			{
				if (item != null) 
				{
					if (item.gameObject.CompareTag("Enemy")) 
					{
						item.GetComponent<MonsterData> ().monsterHp -= item.GetComponent<MonsterData> ().monsterMaxHp * .3f;
					}
					else if (item.gameObject.CompareTag("Boss")) 
					{
						item.GetComponent<MonsterData> ().monsterHp -= item.GetComponent<MonsterData> ().monsterMaxHp * .1f;
					}
				}
			}

		}
		base.updateSkill ();
	}


}
