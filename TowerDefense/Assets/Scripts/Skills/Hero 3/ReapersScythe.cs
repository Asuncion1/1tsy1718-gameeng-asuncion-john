﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName="Skills/Invocromancer/Reapers Scythe")]
public class ReapersScythe : Skill {
	float delay = 2;
	public override void Effect ()
	{
		if (target != null) 
		{
			target.GetComponent<MonsterData> ().monsterHp -= target.GetComponent<MonsterData> ().monsterMaxHp;
			Debug.Log ("Hit" + target.name);
		}
		base.Effect ();
	}
	public override void updateSkill ()
	{
		ray = Camera.main.ViewportPointToRay (Camera.main.ScreenToViewportPoint (Input.mousePosition));
		if (Physics.Raycast (ray, out hit, Mathf.Infinity)) 
		{
			if (hit.collider.tag == "Enemy") 
			{
				target = hit.collider.gameObject;
				Debug.Log (hit.collider.tag);
			} 
			else 
			{
				
				if (delay > 0) 
				{
					delay -= Time.deltaTime;
					Debug.Log (delay);
				} 
				else 
				{
					target = null;
					delay = 5;
				}

			}
		}
		base.updateSkill ();
	}
}
