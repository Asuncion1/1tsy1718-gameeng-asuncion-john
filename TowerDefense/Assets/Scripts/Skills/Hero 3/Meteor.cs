﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName="Skills/Invocromancer/Meteor")]
public class Meteor : Skill {
	public override void Effect ()
	{
		skillObject = new GameObject ();
		skillObject.transform.position = currentRangePrefab.transform.position;

		targets = Physics.OverlapSphere (currentRangePrefab.transform.position, rangeFinderSize, layer);
		foreach (var item in targets) 
		{
			if (item != null) 
			{
				if (item.gameObject.CompareTag("Enemy")) 
				{
					item.GetComponent<MonsterData> ().monsterHp -= item.GetComponent<MonsterData> ().monsterMaxHp * .85f;
				}
				else if (item.gameObject.CompareTag("Boss")) 
				{
					item.GetComponent<MonsterData> ().monsterHp -= item.GetComponent<MonsterData> ().monsterMaxHp * .10f;
				}
			}
		}

		if (skillObject != null) 
		{
			particleEffects = Instantiate(Resources.Load("Prefab/ExplosionsParticles") as GameObject, skillObject.transform);
			ParticleSystem ps = particleEffects.GetComponent<ParticleSystem> ();
			var sh = ps.shape;
		}
		base.Effect ();
	}
}
