﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class RangeDisplay : MonoBehaviour {
	[Range(0,50)]
	public int segments = 50;
	public float radius;
	LineRenderer line;

	Ray ray;
	RaycastHit hit;
	void Start ()
	{
		line = gameObject.GetComponent<LineRenderer>();

		line.SetVertexCount (segments + 1);
		line.useWorldSpace = false;
		CreatePoints ();
	}

	public delegate void ActiveSkill ();
	public static event ActiveSkill OnActive;

	void Update()
	{
		ray = Camera.main.ViewportPointToRay (Camera.main.ScreenToViewportPoint (Input.mousePosition));
		if (Physics.Raycast (ray, out hit, Mathf.Infinity)) 
		{
			transform.position = (hit.point);
			if (Input.GetMouseButtonUp (0) && !EventSystem.current.IsPointerOverGameObject ()) 
			{
				if(OnActive != null)
					OnActive();
				Destroy (this.gameObject);
			}
		}
			
	}



	void CreatePoints ()
	{
		float x;
		float y;
		float z;
		float angle = 20f;
	
		for (int i = 0; i < (segments + 1); i++)
		{
			x = Mathf.Sin (Mathf.Deg2Rad * angle) * radius;
			z = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;

			line.SetPosition (i,new Vector3(x,0,z) );

			angle += (360f / segments);
		}
	}
	void OnDestroy()
	{
		
	}

}
