﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoreController : MonoBehaviour {
	public int coreHealth;
	public int maxCoreHealth;

	// Use this for initialization
	void Start () {
		coreHealth = maxCoreHealth;

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (coreHealth <= 0) 
		{
			coreHealth = 0;
		}
	}
	void OnTriggerEnter(Collider col)
	{
		if (col.tag.Contains("Enemy") ) 
		{
			coreHealth--;
		}
		if (col.tag.Contains("Boss")) 
		{
			Debug.Log ("BOSS HIT");
			coreHealth -= maxCoreHealth;
		}
		col.gameObject.GetComponent<EnemyController> ().isDead = true;
	}
}
