﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChilledDebuff : Debuff {
	public float slow = 2;
	public GameObject chilledParticle;
	GameObject particleEffect;
	ParticleSystem particleSystem;
	// Use this for initialization
	void Start () 
	{
		
		chilledParticle = Resources.Load("Prefab/Flare") as GameObject;
		ApplyDebuff ();
		ApplyEffect ();


	}
	// Update is called once per frame
	public override void Update () 
	{
		base.Update ();

	
	}
		
	public override void ApplyDebuff()
	{
		this.gameObject.GetComponent<NavMeshAgent> ().speed = slow;
	}

	void ApplyEffect()
	{
		if (!this.transform.GetChild (0).name.Contains("chilledEffect")) 
		{
			particleEffect = Instantiate (chilledParticle) as GameObject;
			particleEffect.name = "chilledEffect";

			particleEffect.transform.SetParent (this.gameObject.transform);
			particleEffect.transform.SetSiblingIndex (0);
			particleEffect.transform.localPosition = Vector3.zero;
			particleSystem = particleEffect.GetComponent <ParticleSystem> ();
		}
	}

	void OnDestroy()
	{

		Destroy(particleEffect);
		this.gameObject.GetComponent<NavMeshAgent> ().speed = this.gameObject.GetComponent<MonsterData>().maxSpeed;
	}
}
