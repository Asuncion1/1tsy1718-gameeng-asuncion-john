﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debuff : MonoBehaviour {
	public enum DebuffType
	{
		chilled,
		burning,
		none
	}
	public float debuffDuration = 10;
	public float timer;
	public virtual void ApplyDebuff ()
	{
		
	}
	public virtual void Update()
	{
		if (timer >= debuffDuration) 
		{
			Destroy (this);
		}
		timer += Time.deltaTime;
	}
}
