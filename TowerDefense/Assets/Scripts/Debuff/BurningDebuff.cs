﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningDebuff : Debuff {
	GameObject particleEffect;
	ParticleSystem particleSystem;
	public GameObject burningParticle;
	// Use this for initialization
	void Start () 
	{
		burningParticle = Resources.Load("Prefab/Red Flare") as GameObject;
		ApplyEffect ();
	}

	// Update is called once per frame
	public override void Update () 
	{
		base.Update ();
		ApplyDebuff ();
	}

	public override void ApplyDebuff()
	{
		this.gameObject.GetComponent<MonsterData> ().monsterHp -= 2 * Time.deltaTime;
	}

	void ApplyEffect()
	{
		if (!this.transform.GetChild (0).name.Contains("burningEffect")) 
		{
			particleEffect = Instantiate (burningParticle) as GameObject;
			particleEffect.name = "burningEffect";

			particleEffect.transform.SetParent (this.gameObject.transform);
			particleEffect.transform.SetSiblingIndex (0);
			particleEffect.transform.localPosition = Vector3.zero;
			particleSystem = particleEffect.GetComponent <ParticleSystem> ();
		}
	}

	void OnDestroy()
	{

		Destroy(particleEffect);
		this.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ().speed = this.gameObject.GetComponent<MonsterData>().maxSpeed;
	}
}
