﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public float speed;
	public float lives;
	public GameObject bullet;
	Vector3 bounds;
	public float offset;
	// Use this for initialization
	void Start () {
		//bullet = GetComponent<GameObject> ();
		bounds = Camera.main.ViewportToWorldPoint(new Vector3(1,1,10));
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxisRaw("Horizontal") <= 0.5 || Input.GetAxisRaw("Horizontal") >= 0.5) 
		{
			this.transform.Translate (Vector3.right * Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime);
		}
		if (Input.GetAxisRaw("Vertical") <= 0.5 || Input.GetAxisRaw("Vertical") >= 0.5) 
		{
			this.transform.Translate (Vector3.up * Input.GetAxisRaw("Vertical") * speed * Time.deltaTime);
		}
		if (Input.GetKeyDown(KeyCode.Space)) {
			Instantiate (bullet, this.transform.position, bullet.transform.rotation);
		}

		this.transform.position = new Vector3 (
			Mathf.Clamp (this.transform.position.x, -bounds.x + offset, bounds.x - offset), 
			Mathf.Clamp (this.transform.position.y, -bounds.y + offset, bounds.y - offset), 
							transform.position.z);
	}
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.name.Contains("Asteroid")) {
			Debug.Log ("collided");
			lives--;
		}
		if (col.gameObject.name.Contains("PowerUp")) {
			lives++;
			Debug.Log ("collided powerup");
		}
	}
}