﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetScroller : MonoBehaviour {
	Renderer renderer;
	public float scrollSpeed;
	Vector2 offsetValue;
	// Use this for initialization
	void Start () {
		renderer = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		renderer.material.SetTextureOffset ("_MainTex", new Vector2 (0, Mathf.Repeat (Time.time * scrollSpeed, 1)));
	}
}
