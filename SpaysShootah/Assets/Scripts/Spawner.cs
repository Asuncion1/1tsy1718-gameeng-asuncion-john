﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
	// Use this for initialization
	//public GameObject itemSpawn;
	public List<GameObject> itemSpawn;
	void Start () {
		StartCoroutine (Spawn ());

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	IEnumerator Spawn()
	{
		while (true) {
			Instantiate (itemSpawn[0], new Vector3(Random.Range (-1, 2), 3, -1), itemSpawn[0].transform.rotation);
			Instantiate (itemSpawn[1], new Vector3(Random.Range (-1, 2), 3, -1), itemSpawn[1].transform.rotation);
			yield return new WaitForSeconds (3);
		}
	
	}
}
