﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
	public float speed;
	Rigidbody rigidbody;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		rigidbody.velocity = Vector3.up * speed;
		Destroy (this.gameObject, 1);
	}
	void OnTriggerEnter(Collider col)
	{	
		if (!col.gameObject.name.Contains ("Player")) 
		{
			Destroy (this.gameObject);
		}

	}
}
