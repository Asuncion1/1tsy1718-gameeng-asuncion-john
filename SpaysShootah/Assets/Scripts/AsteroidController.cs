﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour {
	public float speed;
	Rigidbody rigidbody;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {
		rigidbody.velocity = Vector3.down * speed;
		Destroy (this.gameObject, 4);
	}
	void OnTriggerEnter(Collider col)
	{
		Destroy (this.gameObject);
	}
}
